FROM node:14-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY index.js package*.json yarn.lock ./

RUN yarn install

EXPOSE 5050

CMD [ "yarn", "start" ]
