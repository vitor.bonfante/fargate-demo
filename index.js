const express = require("express");

const app = express();

app.use(express.json());

app.get("/", (req, res) => {
  console.log("Requisição recebida!");
  return res.send("OK from Fargate! Updated from GITLAB CI o/");
});

app.listen(5050, () => {
  console.log("Server is Running!");
});
